
/*
 * ADC sample programs for DocodeModem
 * Copyright (c) 2020 Circuit Desgin,Inc
 * This software is released under the MIT License.
 * http://opensource.org/licenses/mit-license.php
 */

#include <docodemo.h>

DOCODEMO Dm = DOCODEMO(); //かならず定義する

//温度によってカーブが変わるみたい
static float adjust_adVal(float val)
{
  float adj = round(val * 100); // 0.1234 -> 12 -> 0.01200

  if (adj >= 1 && adj < 54)
  {
    adj += 3;
  }
  else if (adj >= 54 && adj < 62)
  {
    adj += 2;
  }
  else if (adj >= 62 && adj < 67)
  {
    adj += 1;
  }
  else if (adj >= 67 && adj < 73)
  {
  }
  else if (adj >= 73 && adj < 145)
  {
    adj -= 1;
  }
  else if (adj >= 145)
  {
    adj -= 2;
  }

  return adj / 100.0;
}

void setup() {

  Dm.begin();//かならず行う

  Dm.LedCtrl(RED_LED, ON);
  Dm.LedCtrl(GREEN_LED, OFF);

  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

}

void loop() {

  float val = Dm.readExADC();//AD値を読みます。

  val = adjust_adVal(val);//必要であれば補正します。
  
  String msg = "Adc: " + String(val,2) +" V";
  SerialDebug.println(msg.c_str());
  delay(3000);
}